<?php
#parse("PHP File Header.php")
#if (${NAMESPACE})

namespace ${NAMESPACE};
#end
#set ($NAME_DACHED = $NAME.replaceAll("Asset", "").replaceAll("([^_A-Z])([A-Z])", "$1-$2").toLowerCase())
#set ($NAME_CLS = $NAME.replaceAll("Asset", "").replaceAll("Widget", ""))

#set ($ASSET_POS = $NAME.length() - 5)
#if ($NAME.substring($ASSET_POS) == 'Asset')
use yii\web\AssetBundle;

#parse("PHP Class Doc Comment.php")
class ${NAME} extends AssetBundle
{
    /**
     * @inherit
     */
    public $css = [
        //'css/${NAME_DACHED}.css',
    ];
    /**
     * @inherit
     */
    public $js = [
        //'js/${NAME_DACHED}.js',
    ];
    /**
     * @inherit
     */
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    /**
     * @inherit
     */
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->sourcePath) {
            $this->sourcePath = __DIR__ . '/assets';
        }
        parent::init();
    }
}
#elseif ($NAME.substring(0, 6) == 'Widget')
use PrivateIT\widgets\bootstrap\AbstractWidget;
//use ${NAMESPACE}\forms\\${NAME_CLS}Form;

/**
 * Class ${NAME}
#if (${NAMESPACE}) * @package ${NAMESPACE}
#end
 * @property ${NAME_CLS}Form $model
 */
class ${NAME} extends AbstractWidget
{
    /**
     * @inheritdoc
     */
    public static function createModel()
    {
        return new ${NAME_CLS}Form;
    }
    /**
     * @inheritdoc
     */
    public function getContent()
    {
        return $this->render('${NAME_DACHED}');
    }
}
#else
#parse("PHP Class Doc Comment.php")
class ${NAME}
{
    
}
#end
